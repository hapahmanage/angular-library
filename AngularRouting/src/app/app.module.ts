import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { ProductData } from './products/product-data';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './home/welcome.component';
import { PageNotFoundComponent } from './page-not-found.component';

/* Feature Modules */
import { ProductModule } from './products/product.module';
import { UserModule } from './user/user.module';
import { MessageModule } from './messages/message.module';
import { RouterModule } from '@angular/router';

/** BrowserModule -> to pull basic directives(ngIf* , ngFor*)
 *  InMemoryWebApiModule.forRoot(ProductData, { delay: 1000 }) -> used instead of backend service
 */
@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    InMemoryWebApiModule.forRoot(ProductData, { delay: 1000 }), // need more clarity 

    /** feature modules */
    ProductModule,
    UserModule,
    MessageModule,
    /** importing Router module 
     * using forRoot([]) so that it can be accesible for entire application  
     * - forRoot([]) takes an array of route definations */
    RouterModule.forRoot([
      { 'path': 'welcome', component: WelcomeComponent },
      { 'path': '', redirectTo: 'welcome', pathMatch: 'full' },
      { 'path': '**', component: PageNotFoundComponent }
    ])
  ],
  declarations: [
    AppComponent,
    WelcomeComponent,
    PageNotFoundComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
