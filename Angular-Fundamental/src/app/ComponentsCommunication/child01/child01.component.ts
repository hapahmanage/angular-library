import { Component, OnInit, Input, Output , EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child01',
  templateUrl: './child01.component.html',
  styleUrls: ['./child01.component.css']
})
export class Child01Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  /** Accepting data from parent with the name childparentdata */
  @Input()
  childparentdata:any ;


  /** Passing data from child to parent with the name childdata */

  @Output()
  childdata_data = new EventEmitter();

  passdata(){
    /** it is triggered by event 
     * Emitted data is accesible by parent using output decorator field */
    this.childdata_data.emit(this.childparentdata);
  }

}
