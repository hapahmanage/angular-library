import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-child02',
  templateUrl: './child02.component.html',
  styleUrls: ['./child02.component.css']
})
export class Child02Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  count: number = 2;

  getcount() {
    this.count = this.count+5
    console.log(this.count);
  }

}
