import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent01',
  templateUrl: './parent01.component.html',
  styleUrls: ['./parent01.component.css']
})
export class Parent01Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  parentdata = {
    name: "venkat",
    id: 4,
    dob: "0110"
  }

  countChangedHandler(data: any) {
    console.log("child data passes : ", data);
  }

}
