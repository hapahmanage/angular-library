import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Parent01Component } from './ComponentsCommunication/parent01/parent01.component';
import { Child01Component } from './ComponentsCommunication/child01/child01.component';
import { Child02Component } from './ComponentsCommunication/child02/child02.component';

@NgModule({
  declarations: [
    AppComponent,
    Parent01Component,
    Child01Component,
    Child02Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
