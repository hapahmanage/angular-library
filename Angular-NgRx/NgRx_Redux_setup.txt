================================== REDUX SETUP AND DETAILS =====================================
### STORE ###
1. Single Container for application state
2. Interact with that store is immutable way,need to replace the entire new state
3. Install the @ngrx/store package
4. Organize the application state by feature, ?why not multiple states
5. Name the feature slice with the feature name for easier management
6. Initialize the store using: 
	- StoreModule.forRoot(reducer)
	- StoreModule.forFeature('featurename',reducer)
	- StoreModule.forFeature('products', 
		{
			productList : listReducer,
			productDate : dataReducer
		}
	  )
#######
import { StoreModule } from '@ngrx/store';
@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forChild(productRoutes),
	StoreModule.forFeature('products', reducer)
  ],
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  bootstrap: [AppComponent]
})
export class ProductModule { }

==============================================================

### ACTION ###
1. An action represents an event
2. Define an action for each event worth tracking
3. Action is an object with a type and optional payload:

####
{
	type: 'TOGGLE_PRODUCT_CODE',
	payload: value
} 

==============================================================

### REDUCER ###
1. Responds to dispached actions
2. Replaces the state tree with new State
3. Build a reducer function (one or more per feature)
4. Implement as a switch with a case per action:
	
	export function reducer(state , action){
	 switch (action.type){
		case 'TOGGLE_PRODUCT_CODE':
		return{ ...state, showProductCode: action.payload }
	 }
	}
	
5.spread the state as needed 

NOTE : '...' means spread operator, which represents previous state 
	    and adds new values to the state and returns new state	

================================================================

### DISPATCHING AN ACTION ###

1. Often done in response to a user action or an operation
2. Inject the store in the constructor 

?????????????

3. call the dispatch method in the store

?????????????????

4. Pass in the action to dispatch 
	
	this.store.dispatch({
		type: 'TOGGLE_PRODUCT_CODE',
		payload: value
	});
	
5. Passed to all reducers

===================================================================


### SUBSCRIBE TO THE STORE ###

1. Often done in the ngOnInit lifecycle hook
2. Inject the store in the constructor

???????????

3. Use the store's select operator,passing in the desired slice of state
4. Subscribe : 

	this.store.pipe(select('products')).subscribe(
	products => this.displayCode = products.showProductCode
	);

5. Receive notifications when the state changes

====================================================================














		




































